package listener;

import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import util.EMBEDS;
import util.STATICS;



public class Nick extends ListenerAdapter {

    @Override
    public void onPrivateMessageReceived(PrivateMessageReceivedEvent event){
        if (! event.getAuthor().getId().equals("151681624436768768")) return;
        String contentRaw = event.getMessage().getContentRaw();
        boolean write = contentRaw.startsWith("write");
        if (write){
            String[] writes = contentRaw.split("write");
            STATICS.getJDA().getGuilds().get(0).getTextChannelById("240085523060752384").sendMessage(EMBEDS.nickEmbed(writes[1]).build()).complete();
        }
    }


}
