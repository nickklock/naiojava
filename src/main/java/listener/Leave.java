package listener;


import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberLeaveEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberRemoveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;
import java.awt.*;

public class Leave extends ListenerAdapter {

    @Override
    public void onGuildMemberRemove(@Nonnull GuildMemberRemoveEvent event) {
        TextChannel defaultChannel = event.getGuild().getTextChannelsByName("general",false).get(0);
        EmbedBuilder builder = new EmbedBuilder()
                .setColor(Color.RED)
                .setDescription(event.getUser().getName()+" just left the server.");

        assert defaultChannel != null;

        defaultChannel.sendMessage(builder.build()).queue();    }


}
