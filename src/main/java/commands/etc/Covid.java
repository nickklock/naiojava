package commands.etc;

import commands.Command;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import util.STATICS;

import java.io.IOException;
import java.text.ParseException;
import java.util.Objects;

public class Covid implements Command {
    @Override
    public boolean called(String[] args, MessageReceivedEvent event) {
        return false;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException {
        if (!STATICS.isEnable_covid())return;

        if (args[0].equals("global")){
            try {
                JSONArray globalArray = getGlobal();
                JSONObject global = (JSONObject) globalArray.get(0);

                event.getTextChannel().sendMessage(message(global,"Global").build()).complete();
            } catch (org.json.simple.parser.ParseException e) {
                e.printStackTrace();
            }

        }else{
            try {
                JSONArray countryArray = getCountryJson(args[0]);
                JSONObject country = (JSONObject) countryArray.get(0);

                event.getTextChannel().sendMessage(message(country,args[0]).build()).complete();

            } catch (org.json.simple.parser.ParseException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event) {

    }

    @Override
    public String help() {
        return null;
    }

    @Override
    public String description() {
        return null;
    }

    @Override
    public String commandType() {
        return null;
    }

    @Override
    public int permission() {
        return 0;
    }

    private static JSONArray getGlobal() throws IOException, org.json.simple.parser.ParseException {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url("https://covid-19-data.p.rapidapi.com/totals?format=json")
                .get()
                .addHeader("x-rapidapi-host", "covid-19-data.p.rapidapi.com")
                .addHeader("x-rapidapi-key", "oL2VwxDGPImshKvpVoQ92eCPDI42p1e9FJ8jsnL0Aq7kwSPx7l")
                .build();
        Response response = client.newCall(request).execute();
        String stringresponse = Objects.requireNonNull(response.body()).string();
        JSONParser parser = new JSONParser();
        return (JSONArray) parser.parse(stringresponse);
    }

    private static JSONArray getCountryJson(String country) throws IOException, org.json.simple.parser.ParseException {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url("https://covid-19-data.p.rapidapi.com/country?format=json&name="+country)
                .get()
                .addHeader("x-rapidapi-host", "covid-19-data.p.rapidapi.com")
                .addHeader("x-rapidapi-key", "oL2VwxDGPImshKvpVoQ92eCPDI42p1e9FJ8jsnL0Aq7kwSPx7l")
                .build();

        Response response = client.newCall(request).execute();
        String stringresponse = Objects.requireNonNull(response.body()).string();
        JSONParser parser = new JSONParser();
        return (JSONArray) parser.parse(stringresponse);
    }

    private static EmbedBuilder message(JSONObject o, String c){
        EmbedBuilder msg = new EmbedBuilder().setTitle(c.toUpperCase()+" Covid-19 stats");
        msg.addField(":microbe: Confirmed Cases",o.get("confirmed").toString(),true);
        msg.addBlankField(true);
        msg.addField(":skull: Critical",o.get("critical").toString(),true);
        msg.addField(":skull_crossbones: Total Deaths",o.get("deaths").toString(),true);
        msg.addBlankField(true);
        msg.addField(":pill: Total Recovered",o.get("recovered").toString(),true);
        msg.addBlankField(true);

        return msg;
    }

}
