package commands.sro;

import commands.Command;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import util.EMBEDS;
import util.CMD_REACTION;
import util.STATICS;

import java.awt.*;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Raffle implements Command{
    private static LinkedList participants;
    private static LinkedList participants2;
    private static boolean multi;
    private static ArrayList<String> winners;

    @Override
    public boolean called(String[] args, MessageReceivedEvent event){
        return false;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException{
        event.getTextChannel().sendTyping().complete();

        if (!event.getTextChannel().getName().equals(STATICS.RAFFLE_CHANNEL)){
            String asMention = event.getGuild().getTextChannelsByName(STATICS.RAFFLE_CHANNEL, false).get(0).getAsMention();
            event.getTextChannel().sendMessage(EMBEDS.errorEmbed("Wrong text-channel"," Only usable in "+asMention+" .").build()).complete();
            CMD_REACTION.negative(event);
            return;
        }
        if (args.length < 1){
            event.getTextChannel().sendMessage(EMBEDS.helpEmbed(help(),description()).build()).complete();
            CMD_REACTION.negative(event);
            return;
        }

        participants = new LinkedList<>(Arrays.asList(args));
        participants2 = new LinkedList<>(Arrays.asList(args));
        winners = new ArrayList<>();
        CMD_REACTION.positive(event);
        switch (String.valueOf(isInt(args[0]))){
            case "true":
                int howMany = Integer.valueOf(args[0]);
                participants.remove(args[0]);
                participants2.remove(args[0]);
                multi = true;

                if (howMany > participants.size()){
                    System.out.println("more times then participants");
                    System.out.println(participants2);
                    int leftoverTimes = howMany - participants.size();

                    for (int i = 1; i <= howMany; i++){
                        EmbedBuilder message = createMessage();
                        if (message != null){
                            event.getTextChannel().sendMessage(message.build()).complete();
                        }
                    }

                    participants = participants2;
                    System.out.println(participants2);
                    for (int i = 1; i <= leftoverTimes; i++){
                        EmbedBuilder message = createMessage();
                        if (message != null) {
                            event.getTextChannel().sendMessage(message.build()).complete();
                        }
                    }
                    multi = false;

                }else{
                    for (int i = 1; i <= howMany; i++){
                        event.getTextChannel().sendMessage(createMessage().build()).complete();
                    }
                    multi = false;
                }
                PrivateChannel privateChannel = event.getAuthor().openPrivateChannel().complete();
                privateChannel.sendMessage(createPM().build()).complete();
                privateChannel.close().complete();
                break;


            case "false":
                if (participants.size() < 23) {
                    event.getTextChannel().sendMessage(createMessage().build()).complete();
                }else{
                    event.getTextChannel().sendMessage(createMessage().build()).complete();
                }
                PrivateChannel privateChannel2 = event.getAuthor().openPrivateChannel().complete();
                privateChannel2.sendMessage(createPM().build()).complete();
                privateChannel2.close().complete();

                break;
        }

    }

    private static EmbedBuilder createMessage(){
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.LLLL.yyyy HH:mm");
        String formattedString = localDateTime.format(formatter);
        StringBuilder list = new StringBuilder();

        for (int a = 0 ; a < participants.size(); a++){
            int ii = a+1;
            list.append("#").append(ii).append(" - ").append(participants.get(a)).append("\n");
        }

        EmbedBuilder msg = new EmbedBuilder()
                .setColor(Color.YELLOW)
                .setTitle("Participants")
                .setFooter("Raffle date: "+formattedString,null)
                .setDescription(list);

        msg.addBlankField(false);

        if (participants.size() == 0) return null ;
        int randomNum = ThreadLocalRandom.current().nextInt(0, participants.size() );
        msg.addField("WINNER","Number: "+(randomNum+1) + " | Name: "+participants.get(randomNum),false);
        winners.add(participants.get(randomNum).toString());
        if (multi) {
            participants.remove(randomNum);
        }


        return msg;
    }

    private static EmbedBuilder createPM(){
        StringBuilder winner = new StringBuilder();

        for (String a :winners) {
            winner.append("\n").append(a);
        }

        return new EmbedBuilder().setColor(Color.GREEN).setDescription(winner.toString());
    }
    /*
    private static EmbedBuilder createMessage(String list){
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.LLLL.yyyy HH:mm");
        String formattedString = localDateTime.format(formatter);

        EmbedBuilder msg = new EmbedBuilder()
                .setColor(Color.YELLOW)
                .setTitle("Participants")
                .setFooter("Raffle date: "+formattedString,null)
                .setDescription(list);

        msg.addBlankField(false);
        int randomNum = ThreadLocalRandom.current().nextInt(0, participants.size() );
        msg.addField("WINNER","Number: "+(randomNum+1) + " | Name: "+participants.get(randomNum),false);

        if (multi) {
            participants.remove(randomNum);
        }


        return msg;
    }*/


    @Override
    public void executed(boolean success, MessageReceivedEvent event){
        System.out.println("Executed Raffle Command " + !success + " from " +event.getMember().getUser().getName()+"#"+event.getMember().getUser().getDiscriminator());
    }

    @Override
    public String help(){
        return "**.r/.raffle** name1 name2 name3 ...\n" +
                "**.r/raffle** [Amount of raffles] name1 name2 name3 ...";
    }

    @Override
    public String description(){
        return "Generates a list from the input and generates a random number. And announces the winner. Can only be used in #raffle";
    }

    @Override
    public String commandType(){
        return null;
    }

    @Override
    public int permission(){
        return 0;
    }

    private static boolean isInt(String a){
        try{
            int b = Integer.valueOf(a);
            return true;
        }catch (NumberFormatException e){
            return false;
        }
    }

    public static class Raffle2 implements Command{
        private  LinkedList participants;
        private  LinkedList participants2;
        private  boolean multi;
        private  ArrayList<String> winners;

        @Override
        public boolean called(String[] args, MessageReceivedEvent event){
            return false;
        }

        @Override
        public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException{
            if (!STATICS.isEnable_raffle())return;


            event.getTextChannel().sendTyping().complete();

            if (args[0].equals("edit")){

                String remove = args[1];

                TextChannel latestMessageId = event.getTextChannel();
                List<Message> complete = latestMessageId.getHistory().retrievePast(2).complete();

                String id = complete.get(1).getId();

                Message complete1 = latestMessageId.retrieveMessageById(id).complete();
                List<MessageEmbed> embeds = complete1.getEmbeds();
                List<MessageEmbed.Field> oldfields = embeds.get(0).getFields();
                EmbedBuilder newEmbed = new EmbedBuilder().setColor(Color.YELLOW).setTitle("Raffle Winners");
                for (int i = 0 ; i<oldfields.size(); i++){
                    if (oldfields.get(i).getValue().contains(remove)) {
                        MessageEmbed.Field editfield = oldfields.get(i);
                        String replace = editfield.getValue().replace(remove, "");
                        newEmbed.addField(editfield.getName(),replace,false);
                    }else{
                        newEmbed.addField(oldfields.get(i));
                    }

                }
                event.getTextChannel().editMessageById(id,newEmbed.build()).complete();
                event.getMessage().delete().complete();
                return;
            }


            if (!event.getTextChannel().getName().equals(STATICS.RAFFLE_CHANNEL)){
                String asMention = event.getGuild().getTextChannelsByName("raffle", false).get(0).getAsMention();
                event.getTextChannel().sendMessage(EMBEDS.errorEmbed("Wrong text-channel"," Only usable in "+asMention+" .").build()).complete();
                CMD_REACTION.negative(event);
                return;
            }
            if (args.length < 1){
                event.getTextChannel().sendMessage(EMBEDS.helpEmbed(help(),description()).build()).complete();
                CMD_REACTION.negative(event);
                return;
            }




            participants = new LinkedList<>(Arrays.asList(args));
            participants2 = new LinkedList<>(Arrays.asList(args));
            winners = new ArrayList<>();

            CMD_REACTION.positive(event);
            switch (String.valueOf(isInt(args[4]))){
                case "true":
                    for(int i = 0; i <= 4; i++){
                        participants.remove(args[i]);
                        participants2.remove(args[i]);
                    }
                    int howMany = Integer.valueOf(args[4]);
                    multi = true;

                    if (howMany > participants.size()){
                        System.out.println("more times then participants");
                        System.out.println(participants2);
                        int leftoverTimes = howMany - participants.size();

                        for (int i = 1; i <= howMany; i++){
                            EmbedBuilder message = createMessage();
                            if (message != null){
                                event.getTextChannel().sendMessage(message.build()).complete();
                            }
                        }

                        participants = participants2;
                        System.out.println(participants2);
                        for (int i = 1; i <= leftoverTimes; i++){
                            EmbedBuilder message = createMessage();
                            if (message != null) {
                                event.getTextChannel().sendMessage(message.build()).complete();
                            }
                        }
                        multi = false;

                    }else{
                        for (int i = 1; i <= howMany; i++){
                            event.getTextChannel().sendMessage(createMessage().build()).complete();
                        }
                        multi = false;
                    }
                    event.getGuild().getTextChannelById("573259520877264930").sendMessage(winnersMessage(args,event).build()).complete();

                    break;


                case "false":
                    if (participants.size() < 23) {
                        event.getTextChannel().sendMessage(createMessage().build()).complete();
                    }else{
                        event.getTextChannel().sendMessage(createMessage().build()).complete();
                    }
                    event.getGuild().getTextChannelById("573259520877264930").sendMessage(winnersMessage(args,event).build()).complete();

                    break;
            }

        }

        private EmbedBuilder createMessage(){
            LocalDateTime localDateTime = LocalDateTime.now();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.LLLL.yyyy HH:mm");
            String formattedString = localDateTime.format(formatter);
            StringBuilder list = new StringBuilder();

            for (int a = 0 ; a < participants.size(); a++){
                int ii = a+1;
                list.append("#").append(ii).append(" - ").append(participants.get(a)).append("\n");
            }

            EmbedBuilder msg = new EmbedBuilder()
                    .setColor(Color.YELLOW)
                    .setTitle("Participants")
                    .setFooter("Raffle date: "+formattedString,null)
                    .setDescription(list);

            msg.addBlankField(false);

            if (participants.size() == 0) return null ;
            int randomNum = ThreadLocalRandom.current().nextInt(0, participants.size() );
            msg.addField("WINNER","Number: "+(randomNum+1) + " | Name: "+participants.get(randomNum),false);
            winners.add(participants.get(randomNum).toString());

            if (multi) {
                participants.remove(randomNum);
            }


            return msg;
        }


        private EmbedBuilder winnersMessage(String[] args, MessageReceivedEvent event){
            System.out.println(winners);
            int dropAmount1 = Integer.parseInt(args[0]);
            String dropName1 = args[1];

            String dropName2 = args[3];

            String drop1names = "";
            int a = 0;
            for ( int i = 0; i < dropAmount1; i++){
                drop1names = drop1names+winners.get(i)+"\n";
                a = i;
            }
            String drop2names = "";
            for (int i = a+1; i < winners.size(); i++){
                drop2names = drop2names+winners.get(i)+"\n";
            }

            return new EmbedBuilder().addField(dropName1,drop1names,false).addField(dropName2,drop2names,false)
                    .setColor(Color.YELLOW).setTitle("Raffle Winners").setFooter("Raffled by "+ event.getMember().getNickname(),null);



        }


        @Override
        public void executed(boolean success, MessageReceivedEvent event){
            System.out.println("Executed Raffle Command " + !success + " from " +event.getMember().getUser().getName()+"#"+event.getMember().getUser().getDiscriminator());
        }

        @Override
        public String help(){
            return "**.r/.raffle** name1 name2 name3 ...\n" +
                    "**.r/raffle** [Amount of raffles] name1 name2 name3 ...";
        }

        @Override
        public String description(){
            return "Generates a list from the input and generates a random number. And announces the winner. Can only be used in #raffle";
        }

        @Override
        public String commandType(){
            return null;
        }

        @Override
        public int permission(){
            return 0;
        }

        private boolean isInt(String a){
            try{
                int b = Integer.valueOf(a);
                return true;
            }catch (NumberFormatException e){
                return false;
            }
        }
    }

}
