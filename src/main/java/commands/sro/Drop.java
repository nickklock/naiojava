package commands.sro;

import commands.Command;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import util.CMD_REACTION;

import java.awt.*;
import java.io.IOException;
import java.text.ParseException;

public class Drop implements Command{
    @Override
    public boolean called(String[] args, MessageReceivedEvent event){
        return false;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException{
        event.getTextChannel().sendTyping().complete();
        Message message = event.getMessage();

        if (args.length < 1){
            CMD_REACTION.negative(event);
            event.getTextChannel().sendMessage
                    (new EmbedBuilder().setColor(Color.RED).setDescription(help()).build()).complete();

            return;
        }

        CMD_REACTION.positive(event);

        String ss = args[0];
        EmbedBuilder finalMsg = new EmbedBuilder().setColor(Color.GREEN).setTitle("BOX DROP");
        finalMsg.setImage(ss);

        event.getGuild().getTextChannelById("689561784280940558").sendMessage(ss).complete();
        message.delete().complete();
    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event){

    }

    @Override
    public String help(){
        return "Missing argument. Add a Link";
    }

    @Override
    public String description(){
        return null;
    }

    @Override
    public String commandType(){
        return null;
    }

    @Override
    public int permission(){
        return 0;
    }
}
