package commands.settings;

import commands.Command;
import core.json.Config;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import util.CMD_REACTION;
import util.EMBEDS;
import util.Power;

import java.awt.*;
import java.io.IOException;
import java.text.ParseException;
import java.util.Objects;

public class Admin implements Command {
    @Override
    public boolean called(String[] args, MessageReceivedEvent event) {
        return false;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) throws ParseException, IOException {
       if (Power.noPower(Objects.requireNonNull(event.getMember()))) return;

        if (args.length < 2){
            CMD_REACTION.negative(event);
            event.getTextChannel().sendMessage(EMBEDS.helpEmbed(help(),description()).build()).complete();
            return;
        }
        switch (args[0]){
            case "enable":
                if (args[1].equals("clear")){
                    Config.enableClear(true);
                    event.getTextChannel().sendMessage(responseEnable(args[0],args[1]).build()).complete();;
                    CMD_REACTION.positive(event);
                }
                if (args[1].equals("help")){
                    Config.enableHelp(true);
                    event.getTextChannel().sendMessage(responseEnable(args[0],args[1]).build()).complete();;
                    CMD_REACTION.positive(event);
                }
                if (args[1].equals("triggers")){
                    Config.enableTriggers(true);
                    event.getTextChannel().sendMessage(responseEnable(args[0],args[1]).build()).complete();;
                    CMD_REACTION.positive(event);
                }
                if (args[1].equals("urban")){
                    Config.enableUrban(true);
                    event.getTextChannel().sendMessage(responseEnable(args[0],args[1]).build()).complete();;
                    CMD_REACTION.positive(event);
                }
                if (args[1].equals("userinfo")){
                    Config.enableUserInfo(true);
                    event.getTextChannel().sendMessage(responseEnable(args[0],args[1]).build()).complete();;
                    CMD_REACTION.positive(event);
                }
                if (args[1].equals("roles")){
                    Config.enableRoles(true);
                    event.getTextChannel().sendMessage(responseEnable(args[0],args[1]).build()).complete();;
                    CMD_REACTION.positive(event);
                }
                if (args[1].equals("birthday")){
                    Config.enableBirthday(true);
                    event.getTextChannel().sendMessage(responseEnable(args[0],args[1]).build()).complete();;
                    CMD_REACTION.positive(event);

                }
                if (args[1].equals("covid")){
                    Config.enableCovid(true);
                    event.getTextChannel().sendMessage(responseEnable(args[0],args[1]).build()).complete();;
                    CMD_REACTION.positive(event);

                }
                if (args[1].equals("crypto")){
                    Config.enableCrypto(true);
                    event.getTextChannel().sendMessage(responseEnable(args[0],args[1]).build()).complete();;
                    CMD_REACTION.positive(event);

                }
                if (args[1].equals("music")){
                    Config.enableMusic(true);
                    event.getTextChannel().sendMessage(responseEnable(args[0],args[1]).build()).complete();;
                    CMD_REACTION.positive(event);

                }
                break;

            case "disable":
                if (args[1].equals("clear")){
                    Config.enableClear(false);
                    event.getTextChannel().sendMessage(responseEnable(args[0],args[1]).build()).complete();;
                    CMD_REACTION.positive(event);

                }
                if (args[1].equals("help")){
                    Config.enableHelp(false);
                    event.getTextChannel().sendMessage(responseEnable(args[0],args[1]).build()).complete();;
                    CMD_REACTION.positive(event);

                }
                if (args[1].equals("triggers")){
                    Config.enableTriggers(false);
                    event.getTextChannel().sendMessage(responseEnable(args[0],args[1]).build()).complete();;
                    CMD_REACTION.positive(event);

                }
                if (args[1].equals("urban")){
                    Config.enableUrban(false);
                    event.getTextChannel().sendMessage(responseEnable(args[0],args[1]).build()).complete();;
                    CMD_REACTION.positive(event);

                }
                if (args[1].equals("userinfo")){
                    Config.enableUserInfo(false);
                    event.getTextChannel().sendMessage(responseEnable(args[0],args[1]).build()).complete();;
                    CMD_REACTION.positive(event);

                }
                if (args[1].equals("roles")){
                    Config.enableRoles(false);
                    event.getTextChannel().sendMessage(responseEnable(args[0],args[1]).build()).complete();;
                    CMD_REACTION.positive(event);

                }
                if (args[1].equals("birthday")){
                    Config.enableBirthday(false);
                    event.getTextChannel().sendMessage(responseEnable(args[0],args[1]).build()).complete();;
                    CMD_REACTION.positive(event);

                }
                if (args[1].equals("covid")){
                    Config.enableCovid(false);
                    event.getTextChannel().sendMessage(responseEnable(args[0],args[1]).build()).complete();;
                    CMD_REACTION.positive(event);

                }
                if (args[1].equals("crypto")){
                    Config.enableCrypto(false);
                    event.getTextChannel().sendMessage(responseEnable(args[0],args[1]).build()).complete();;
                    CMD_REACTION.positive(event);

                }
                if (args[1].equals("music")){
                    Config.enableMusic(false);
                    event.getTextChannel().sendMessage(responseEnable(args[0],args[1]).build()).complete();;
                    CMD_REACTION.positive(event);

                }
                if (args[1].equals("raffle")){
                    Config.enableRaffle(false);
                    event.getTextChannel().sendMessage(responseEnable(args[0],args[1]).build()).complete();
                    CMD_REACTION.positive(event);

                }
                break;

            case "change":
                if (args[1].equals("raffle")){
                    Config.setRaffleChannel(args[2]);
                    CMD_REACTION.positive(event);
                    event.getTextChannel().sendMessage(responseChange(args[0],args[1],args[2]).build()).complete();

                }
                if (args[1].equals("music")){
                    Config.setMusicChannel(args[2]);
                    CMD_REACTION.positive(event);
                    event.getTextChannel().sendMessage(responseChange(args[0],args[1],args[2]).build()).complete();

                }
                if (args[1].equals("voicelog")){
                    Config.setVoicelogChannel(args[2]);
                    CMD_REACTION.positive(event);
                    event.getTextChannel().sendMessage(responseChange(args[0],args[1],args[2]).build()).complete();

                }
                if (args[1].equals("crypto")){
                    Config.setCryptoPrefix(args[2]);
                    CMD_REACTION.positive(event);
                    event.getTextChannel().sendMessage(responseChange(args[0],args[1],args[2]).build()).complete();

                }
                break;
            default:
                System.out.println("smth wrong");
        }
    }

    private static EmbedBuilder responseEnable(String ed, String what){
        return new EmbedBuilder().setColor(Color.GREEN)
                .setTitle("Executed Admin Command")
                .setDescription(ed+"d "+ what +" command.");
    }

    private static EmbedBuilder responseChange(String ed, String what, String to){
        return new EmbedBuilder().setColor(Color.GREEN)
                .setTitle("Executed Admin Command")
                .setDescription(ed+"d "+what+"channel to "+ to+".");
    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event) {

    }

    @Override
    public String help() {
        return "**.a / .admin enable/disable command ** *command can be clear, help, triggers, urban, userinfo, role, " +
                "birthday, covid, crypto, music*\n" +
                "**.a / .admin change textchannel** *textchannel can be voicelog, music, raffle*";
    }

    @Override
    public String description() {
        return "People with power roles can modify the bots config.";
    }

    @Override
    public String commandType() {
        return null;
    }

    @Override
    public int permission() {
        return 0;
    }
}
