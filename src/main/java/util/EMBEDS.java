package util;


import net.dv8tion.jda.api.EmbedBuilder;

import java.awt.*;

public class EMBEDS{

    public static EmbedBuilder helpEmbed(String help, String desc){

        return new EmbedBuilder().setColor(Color.CYAN)
                .setTitle("Help")
                .setDescription(desc)
                .addField("Instructions",help,false);
    }

    public static EmbedBuilder errorEmbed(String title, String msg){
        return new EmbedBuilder()
                .setColor(Color.RED)
                .setTitle("Error - " + title)
                .setDescription(msg);
    }

    public static EmbedBuilder nopowerEmbed(){
        return new EmbedBuilder()
                .setDescription("You cant use that feature.")
                .setTitle("Power - Error")
                .setColor(Color.RED)
                ;
    }


    public static EmbedBuilder nickEmbed(String s){
        return new EmbedBuilder()
                .setDescription(s) ;
    }


}
