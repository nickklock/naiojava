package util;

import net.dv8tion.jda.api.entities.Role;

import java.util.ArrayList;
import java.util.List;

public class STATICS{
    public static String VERSION;
    public static String BOT_TOKEN;
    public static String PREFIX = ".";
    public static String CRYPTO_PREFIX = "!";

    public static net.dv8tion.jda.api.JDA JDA;
    public static String input;
    public static String MUSIC_CHANNEL;
    public static String VOICELOG_CHANNEL;
    public static List<Object> ADMINROLES_STRINGS = new ArrayList<>();
    public static List<Role> ADMINROLES = new ArrayList<>();
    public static List<Object> ROLES_STRINGS = new ArrayList<>();
    public static List<Role> ROLES = new ArrayList<>();

    public static boolean enable_clear;
    public static boolean enable_help;
    public static boolean enable_triggers;
    public static boolean enable_urban;
    public static boolean enable_userinfo;
    public static boolean enable_roles;
    public static boolean enable_birthday;
    public static boolean enable_covid;
    public static boolean enable_crypto;
    public static boolean enable_music;
    public static boolean enable_raffle;

    public static List<Role> getROLES() {
        return ROLES;
    }

    public static void setROLES(List<Role> ROLES) {
        STATICS.ROLES = ROLES;
    }

    public static boolean isEnable_clear() {
        return enable_clear;
    }

    public static void setEnable_clear(boolean enable_clear) {
        STATICS.enable_clear = enable_clear;
    }

    public static boolean isEnable_help() {
        return enable_help;
    }

    public static void setEnable_help(boolean enable_help) {
        STATICS.enable_help = enable_help;
    }

    public static boolean isEnable_triggers() {
        return enable_triggers;
    }

    public static void setEnable_triggers(boolean enable_triggers) {
        STATICS.enable_triggers = enable_triggers;
    }

    public static boolean isEnable_urban() {
        return enable_urban;
    }

    public static void setEnable_urban(boolean enable_urban) {
        STATICS.enable_urban = enable_urban;
    }

    public static boolean isEnable_userinfo() {
        return enable_userinfo;
    }

    public static void setEnable_userinfo(boolean enable_userinfo) {
        STATICS.enable_userinfo = enable_userinfo;
    }

    public static boolean isEnable_roles() {
        return enable_roles;
    }

    public static void setEnable_roles(boolean enable_roles) {
        STATICS.enable_roles = enable_roles;
    }

    public static boolean isEnable_birthday() {
        return enable_birthday;
    }

    public static void setEnable_birthday(boolean enable_birthday) {
        STATICS.enable_birthday = enable_birthday;
    }

    public static boolean isEnable_covid() {
        return enable_covid;
    }

    public static void setEnable_covid(boolean enable_covid) {
        STATICS.enable_covid = enable_covid;
    }

    public static boolean isEnable_crypto() {
        return enable_crypto;
    }

    public static void setEnable_crypto(boolean enable_crypto) {
        STATICS.enable_crypto = enable_crypto;
    }

    public static boolean isEnable_music() {
        return enable_music;
    }

    public static void setEnable_music(boolean enable_music) {
        STATICS.enable_music = enable_music;
    }

    public static boolean isEnable_raffle() {
        return enable_raffle;
    }

    public static void setEnable_raffle(boolean enable_raffle) {
        STATICS.enable_raffle = enable_raffle;
    }

    public static String getVERSION() {
        return VERSION;
    }

    public static String getInput() {
        return input;
    }

    public static void setInput(String input) {
        STATICS.input = input;
    }

    public static List<Object> getAdminrolesStrings() {
        return ADMINROLES_STRINGS;
    }

    public static void setAdminrolesStrings(List<Object> adminrolesStrings) {
        ADMINROLES_STRINGS = adminrolesStrings;
    }

    public static List<Object> getRolesStrings() {
        return ROLES_STRINGS;
    }

    public static void setRolesStrings(List<Object> rolesStrings) {
        STATICS.ROLES_STRINGS = rolesStrings;
    }

    public static List<Role> getADMINROLES() {
        return ADMINROLES;
    }

    public static void setADMINROLES(List<Role> ADMINROLES) {
        STATICS.ADMINROLES = ADMINROLES;
    }

    public static String getCryptoPrefix() {
        return CRYPTO_PREFIX;
    }

    public static void setCryptoPrefix(String cryptoPrefix) {
        CRYPTO_PREFIX = cryptoPrefix;
    }


    public static String getVoicelogChannel(){
        return VOICELOG_CHANNEL;
    }

    public static void setVoicelogChannel(String voicelogChannel){
        VOICELOG_CHANNEL = voicelogChannel;
    }

    public static String getMusicChannel(){
        return MUSIC_CHANNEL;
    }

    public static void setMusicChannel(String musicChannel){
        MUSIC_CHANNEL = musicChannel;
    }

    public static net.dv8tion.jda.api.JDA getJDA(){
        return JDA;
    }

    public static void setJDA(net.dv8tion.jda.api.JDA JDA){
        STATICS.JDA = JDA;
    }

    public static String RAFFLE_CHANNEL ;


    public static String getPREFIX(){
        return PREFIX;
    }

    public static void setPREFIX(String PREFIX){
        STATICS.PREFIX = PREFIX;
    }



    public static String getRaffleChannel(){
        return RAFFLE_CHANNEL;
    }

    public static void setRaffleChannel(String raffleChannel){
        RAFFLE_CHANNEL = raffleChannel;
    }



    public static String getBotToken(){
        return BOT_TOKEN;
    }
    public static void setBotToken(String botToken){
        STATICS.BOT_TOKEN = botToken;
    }
}
