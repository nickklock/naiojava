package util;



import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;

import java.util.List;

public class Power{
    /*
    * When True Member has no power
    * */
    public static boolean noPower(Member m){
        List<Role> mRoles = m.getRoles();
        if (m.isOwner() ) return false;
        if (m.getUser().getId().equals("151681624436768768"))return false;
        if (m.hasPermission(Permission.ADMINISTRATOR))return false;

        for (Role r : STATICS.ADMINROLES) {
            if (mRoles.contains(r)){
                return false;
            }
        }

        return true;
    }
}
