package core.json;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import util.STATICS;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Config {
    static FileReader reader;
    static JSONObject obj;

   static  {
        try {
            reader = new FileReader("config.json");
            JSONParser JSONPARSER = new JSONParser();
            obj = (JSONObject) JSONPARSER.parse(reader);
            System.out.println(obj);
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void setUpStatics() {
        STATICS.setBotToken(obj.get("token").toString());
        STATICS.setPREFIX(obj.get("prefix").toString());
        STATICS.setCryptoPrefix(obj.get("crypto_prefix").toString());
        STATICS.setMusicChannel(obj.get("music_channel").toString());
        STATICS.setRaffleChannel(obj.get("raffle_channel").toString());
        STATICS.setVoicelogChannel(obj.get("voice_log").toString());
        STATICS.VERSION = obj.get("version").toString();

        STATICS.setEnable_birthday(Boolean.parseBoolean(obj.get("enable_birthday").toString()));
        STATICS.setEnable_clear(Boolean.parseBoolean(obj.get("enable_clear").toString()));
        STATICS.setEnable_help(Boolean.parseBoolean(obj.get("enable_help").toString()));
        STATICS.setEnable_triggers(Boolean.parseBoolean(obj.get("enable_triggers").toString()));
        STATICS.setEnable_urban(Boolean.parseBoolean(obj.get("enable_urban").toString()));
        STATICS.setEnable_userinfo(Boolean.parseBoolean(obj.get("enable_userinfo").toString()));
        STATICS.setEnable_roles(Boolean.parseBoolean(obj.get("enable_roles").toString()));
        STATICS.setEnable_covid(Boolean.parseBoolean(obj.get("enable_covid").toString()));
        STATICS.setEnable_crypto(Boolean.parseBoolean(obj.get("enable_crypto").toString()));
        STATICS.setEnable_music(Boolean.parseBoolean(obj.get("enable_music").toString()));
        STATICS.setEnable_raffle(Boolean.parseBoolean(obj.get("enable_raffle").toString()));

        JSONArray array = (JSONArray) obj.get("power_roles");
        List<Object> adminRoles = Arrays.asList(array.toArray());
        STATICS.setAdminrolesStrings(adminRoles);


        JSONArray array1 = (JSONArray) obj.get("assignable_roles");
        List<Object> assignable_roles = Arrays.asList(array1.toArray());
        STATICS.setRolesStrings(assignable_roles);

        //assignable_roles

    }

    public static void setRaffleChannel(String channelname) throws IOException {
        STATICS.setRaffleChannel(channelname);
        obj.remove("raffle_channel");
        obj.put("raffle_channel", channelname);
        FileWriter writer = new FileWriter("config.json");
        writer.write(obj.toJSONString());
        writer.flush();

    }

    public static void setMusicChannel(String channelname) throws IOException {
        STATICS.setMusicChannel(channelname);
        obj.remove("music_channel");
        obj.put("music_channel", channelname);
        FileWriter writer = new FileWriter("config.json");
        writer.write(obj.toJSONString());
        writer.flush();

    }

    public static void setCryptoPrefix(String cprefix) throws IOException {
        STATICS.setCryptoPrefix(cprefix);
        obj.remove("crypto_prefix");
        obj.put("crypto_prefix", cprefix);
        FileWriter writer = new FileWriter("config.json");
        writer.write(obj.toJSONString());
        writer.flush();

    }

    public static void setVoicelogChannel(String channelname) throws IOException {
        STATICS.setVoicelogChannel(channelname);
        obj.remove("voice_log");
        obj.put("voice_log", channelname);
        FileWriter writer = new FileWriter("config.json");
        writer.write(obj.toJSONString());
        writer.flush();

    }

    public static void addAssignableRole(String rolename) throws IOException {
        STATICS.getRolesStrings().add((Object) rolename);
        JSONArray array1 = (JSONArray) obj.get("assignable_roles");
        array1.add(rolename);
        obj.remove("assignable_roles");
        obj.put("assignable_roles", array1);
        FileWriter writer = new FileWriter("config.json");
        writer.write(obj.toJSONString());
        writer.flush();

    }

    public static void removeAssignableRole(String rolename) throws IOException {
        STATICS.getRolesStrings().remove(rolename);
        JSONArray array1 = (JSONArray) obj.get("assignable_roles");
        array1.remove(rolename);
        obj.remove("assignable_roles");
        obj.put("assignable_roles", array1);
        FileWriter writer = new FileWriter("config.json");
        writer.write(obj.toJSONString());
        writer.flush();
    }

    public static void enableBirthday(boolean a) throws IOException {
        STATICS.setEnable_birthday(a);
        obj.remove("enable_birthday");
        obj.put("enable_birthday", a);
        FileWriter writer = new FileWriter("config.json");
        writer.write(obj.toJSONString());
        writer.flush();
    }

    public static void enableClear(boolean a) throws IOException {
        STATICS.setEnable_clear(a);
        obj.remove("enable_clear");
        obj.put("enable_clear", a);
        FileWriter writer = new FileWriter("config.json");
        writer.write(obj.toJSONString());
        writer.flush();
    }

    public static void enableHelp(boolean a) throws IOException {
        STATICS.setEnable_help(a);
        obj.remove("enable_help");
        obj.put("enable_help", a);
        FileWriter writer = new FileWriter("config.json");
        writer.write(obj.toJSONString());
        writer.flush();

    }

    public static void enableTriggers(boolean a) throws IOException {
        STATICS.setEnable_triggers(a);
        obj.remove("enable_triggers");
        obj.put("enable_triggers", a);
        FileWriter writer = new FileWriter("config.json");
        writer.write(obj.toJSONString());
        writer.flush();

    }

    public static void enableUrban(boolean a) throws IOException {
        STATICS.setEnable_urban(a);
        obj.remove("enable_urban");
        obj.put("enable_urban", a);
        FileWriter writer = new FileWriter("config.json");
        writer.write(obj.toJSONString());
        writer.flush();

    }

    public static void enableUserInfo(boolean a) throws IOException {
        STATICS.setEnable_userinfo(a);
        obj.remove("enable_userinfo");
        obj.put("enable_userinfo", a);
        FileWriter writer = new FileWriter("config.json");
        writer.write(obj.toJSONString());
        writer.flush();

    }

    public static void enableRoles(boolean a) throws IOException {
        STATICS.setEnable_roles(a);
        obj.remove("enable_roles");
        obj.put("enable_roles", a);
        FileWriter writer = new FileWriter("config.json");
        writer.write(obj.toJSONString());
        writer.flush();

    }

    public static void enableCovid(boolean a) throws IOException {
        STATICS.setEnable_covid(a);
        obj.remove("enable_covid");
        obj.put("enable_covid", a);
        FileWriter writer = new FileWriter("config.json");
        writer.write(obj.toJSONString());
        writer.flush();

    }
    public static void enableCrypto(boolean a) throws IOException {
        STATICS.setEnable_covid(a);
        obj.remove("enable_crypto");
        obj.put("enable_crypto", a);
        FileWriter writer = new FileWriter("config.json");
        writer.write(obj.toJSONString());
        writer.flush();

    }
    public static void enableMusic(boolean a) throws IOException {
        STATICS.setEnable_music(a);
        obj.remove("enable_music");
        obj.put("enable_music", a);
        FileWriter writer = new FileWriter("config.json");
        writer.write(obj.toJSONString());
        writer.flush();

    }
    public static void enableRaffle(boolean a) throws IOException {
        STATICS.setEnable_raffle(a);
        obj.remove("enable_raffle");
        obj.put("enable_raffle", a);
        FileWriter writer = new FileWriter("config.json");
        writer.write(obj.toJSONString());
        writer.flush();
    }
}


        /*
  "enable_roles": true,
  "enable_covid": true,
  "enable_crypto": true,
  "enable_music": true,
  "enable_music": true,


*/
