package core.json;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;

public class CustomTriggers {

    public static String sendRespond(MessageReceivedEvent e){
        try {
            JSONParser jsonParser = new JSONParser();

            FileReader reader = new FileReader("triggers.json");
            Object obj = jsonParser.parse(reader);
            JSONObject jsonObject = (JSONObject) obj;
            Object o = jsonObject.get(e.getMessage().getContentRaw());
            if (o != null)
            {
                return o.toString();

            }
        } catch (ParseException | IOException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        }
        return null;
    }

    public static void addTrigger(String trigger, String response) throws IOException, ParseException {

            JSONParser jsonParser = new JSONParser();

            FileReader reader = new FileReader("triggers.json");
            Object obj = jsonParser.parse(reader);
            JSONObject jsonObject = (JSONObject) obj;
            jsonObject.put(trigger, response);

            FileWriter writer = new FileWriter("triggers.json");
            writer.write(jsonObject.toJSONString());
            writer.flush();
    }

    public static void delteTrigger(String trigger) throws IOException, ParseException {

            JSONParser jsonParser = new JSONParser();

            FileReader reader = new FileReader("triggers.json");
            JSONObject obj = (JSONObject) jsonParser.parse(reader);
            Object remove = obj.remove(trigger.trim());
            System.out.println(remove);
            String s = JSONValue.toJSONString(obj);
            FileWriter writer = new FileWriter("triggers.json");
            writer.write(s);
            writer.flush();

    }


    public static String getAllTriggers() throws IOException, ParseException {

        JSONParser jsonParser = new JSONParser();
        FileReader reader = new FileReader("triggers.json");
        Object obj = jsonParser.parse(reader);
        JSONObject jsonObject = (JSONObject) obj;

        StringBuilder builder = new StringBuilder();

        jsonObject.forEach((k,v) ->{
            builder.append(k+"\n");
        });
        return builder.toString();
    }



}
