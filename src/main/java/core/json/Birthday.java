package core.json;

import core.models.Bd;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

public class Birthday {

    public static void addBirthday(MessageReceivedEvent e, LocalDate birthday) throws IOException, ParseException, DuplicateName {

        JSONParser jsonParser = new JSONParser();
        FileReader reader = new FileReader("birthdays.json");
        Object obj = jsonParser.parse(reader);
        JSONObject jsonObject = (JSONObject) obj;
        if (jsonObject.containsKey(e.getMember().getId())) throw new DuplicateName(e.getMessage().getId());
        jsonObject.put(e.getMember().getId(), birthday.toString());

        FileWriter writer = new FileWriter("birthdays.json");
        writer.write(jsonObject.toJSONString());
        writer.flush();
    }

    public static void removeBirthday(String id) throws IOException, ParseException {

        JSONParser jsonParser = new JSONParser();

        FileReader reader = new FileReader("birthdays.json");
        JSONObject obj = (JSONObject) jsonParser.parse(reader);
        obj.remove(id.trim());
        String s = JSONValue.toJSONString(obj);
        FileWriter writer = new FileWriter("birthdays.json");
        writer.write(s);
        writer.flush();
    }

    public static ArrayList<Bd> nextBirthdays() throws IOException, ParseException {
        JSONParser jsonParser = new JSONParser();
        FileReader reader = new FileReader("birthdays.json");
        JSONObject obj = (JSONObject) jsonParser.parse(reader);
        LocalDate now = LocalDate.now();
        ArrayList<Bd> nextBDs = new ArrayList<>();
        obj.forEach((k,v) ->{
            Bd bd = new Bd();
            bd.setId(k.toString());
            bd.setBd(LocalDate.parse(v.toString()));
            LocalDate bdToCheck = bd.getBd();
            if (bdToCheck.getMonthValue() > now.getMonthValue()){
                nextBDs.add(bd);
            }else if (bdToCheck.getMonthValue() == now.getMonthValue()){
                if (bdToCheck.getDayOfMonth() >= now.getDayOfMonth()) {
                    nextBDs.add(bd);
                }
            }
        });
        return nextBDs;
    }

    public static ArrayList<Bd> getTodaysBirthdays() throws IOException, ParseException {
        JSONParser jsonParser = new JSONParser();
        FileReader reader = new FileReader("birthdays.json");
        JSONObject obj = (JSONObject) jsonParser.parse(reader);
        LocalDate now = LocalDate.now();
        ArrayList<Bd> todaysBD = new ArrayList<>();
        obj.forEach((k,v) ->{
            Bd bd = new Bd();
            bd.setId(k.toString());
            bd.setBd(LocalDate.parse(v.toString()));
            LocalDate bdToCheck = bd.getBd();
            if (bdToCheck.getMonthValue() == now.getMonthValue() && bdToCheck.getDayOfMonth() == now.getDayOfMonth()){
                todaysBD.add(bd);
            }
        });
        return todaysBD;
    }

}
